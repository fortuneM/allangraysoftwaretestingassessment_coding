@runAll
Feature: Validate that the cell phone function is working correctly
  @validate
  Scenario Outline: Validate SA Phone Number
    Given I have a cell number "<cellphone>"
    Then Verify that the validateSAPhoneNumber function is working "<cellphone>"

    Examples:
      | cellphone  |  |  |
      | 0659767023 |  |  |
      | 0734567890  |  |  |