package stepdefs;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import objects.PhoneValidationObject;
import org.junit.Assert;

public class StepDef  extends PhoneValidationObject {
    @Given("^I have a cell number \"([^\"]*)\"$")
    public void iHaveACellNumber(String phone) throws Throwable {
        Assert.assertTrue("Unable to get test data ", !phone.isEmpty());
    }

    @Then("^Verify that the validateSAPhoneNumber function is working \"([^\"]*)\"$")
    public void verifyThatTheValidateSAPhoneNumberFunctionIsWorking(String cellNo) throws Throwable {
        Assert.assertTrue("Cellphone number provided is invalid ", validateSAPhoneNumber(cellNo));
    }
}
