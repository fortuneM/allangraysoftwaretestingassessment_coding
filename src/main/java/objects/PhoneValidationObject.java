package objects;

public class PhoneValidationObject {

    public PhoneValidationObject() {
    }

    public static boolean validateSAPhoneNumber(String cellphoneNumber) {
        if(cellphoneNumber.matches("^((?:\\+27|27)|0)(\\d{2})-?(\\d{3})-?(\\d{4})$")){
            return true;
        }else{
            return false;
        }
    }

}
